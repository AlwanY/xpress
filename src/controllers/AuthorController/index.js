import { Author } from '../../models'

class AuthorController {
    static get = async (req, res) => {
        try {
            const findedAllAuthor = await Author.findAll()

            res.status(200).send(findedAllAuthor)
        } catch(error) {
            res.status(500).send(error)
        }
    }

    static create = async (req, res) => {
        try {
            const { name, gender } = req.body

            const findAuthor = await Author.findOne({where: {name}})

            if (findAuthor) {
                return res.status(404).json({message: 'Author already available'})
            }

            const createAuthor = await Author.create({
                name,
                gender
            })

            res.status(200).send({ message: 'succes add author data'})
        } catch(error) {
            res.status(500).send(error)
        }
    }

    static update = async (req, res) => {
        try {
            const { id } = req.params
            const { name, gender } = req.body

            const findAuthor = await Author.findOne({ where: {id}})
            
            if (!findAuthor) {
                return res.status(404).json({ message: 'Author not found'})
            }

            const updatedAuthor = await Author.update({
                name,
                gender
            }, {where: {id}})
            
            res.status(200).json({message: 'Updating Author'})
        } catch(error) {
            res.status(500).json(error)
        }
    }

    static delete = async (req, res) => {
        try {
            const { id } = req.params
        
            const findAuthor = await Author.findOne({ where: {id}})

            if (!findAuthor) {
                return res.status(404).json({message: 'Author not found'})
            }

            const deleted = await Author.destroy({where: { id }})

            res.status(200).json({message: 'Deleted Author'})
        } catch(error) {
            res.status(500).json({message: 'Internal Server Error'})
        }
    }
}

export default AuthorController