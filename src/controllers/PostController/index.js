import { Post } from '../../models'

class PostController {
  static get = async (req, res) => {
    try {
      const data = await Post.findAll()
      res.status(200).send(data) 
    } catch(error) {
      res.status(500).send(error)
    }
  }

  static create = async (req, res) => {
    try {
      const { title, body, authorId } = req.body

      const findPost = await Post.findOne({where: {title}})

      if(findPost) {
        return res.status(403).json({message: 'Post already available'})
      }

      const createPost = await Post.create({
        title,
        body,
        authorId
      })

      res.status(200).send({ succes: 'succes add data'})
    } catch(error) {
      res.status(500).send(error)
    }
  }

  static update = async (req, res) => {
    try {
      const { title, body, authorId } = req.body
      const { id } = req.params

      const findPost = await Post.findOne({where: {id}})

      if(!findPost) {
        res.status(404).json({message: 'Post not found'})
      }

      const updatedPost = await Post.update({
        title,
        body,
        authorId
      }, {where: { id }})
      
      res.status(200).send({message: 'Succes update post'})
    } catch(error) {
      res.status(500).send(error)
    }
  }

  static delete = async (req, res) => {
    try {
      const { id } = req.params
      const data = await Post.destroy({ where: {id}})
      res.status(200).send({ success: 'succes delete data'})
    } catch(error) {
      res.status(500).send(error)
    }
  }

  static getIndexView = (req, res) => {
    
  }
}

export default PostController
