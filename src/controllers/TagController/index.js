import { Tag } from '../../models'

class TagController {
    static get = async (req, res) => {
        try {
            const findedAllTag = await Tag.findAll()
            
            res.status(200).send(findedAllTag)
        } catch(error) {
            res.status(500).json(error)
        }
    }

    static create = async (req, res) => {
        const { name } = req.body

        const findTag = await Tag.findOne({where: {name}})

        if(findTag) {
            return res.status(500).json({message: 'Tag already available'})
        }

        const createTag = await Tag.create({name})

        res.status(200).json({message: 'Succes add tag'})
    }

    static update = async (req, res) => {
        try {
            const { id } = req.params
            const { name } = req.body

            const findTag = await Tag.findOne({where: {id}})
            
            if(!findTag) {
                res.status(404).json({message: 'Tag not found'})
            }

            const updateTag = await Tag.update({
                name
            }, {where: {id}})

            res.status(200).json({message: 'Succes update tag'})
        } catch(error) {
            res.status(500).json(error)
        }
    }

    static delete = async (req, res) => {
        try {
            const { id } = req.params
        
            const findTag = await Tag.findOne({ where: {id}})

            if (!findTag) {
                return res.status(404).json({message: 'Tag not found'})
            }

            const deleted = await Tag.destroy({where: { id }})

            res.status(200).json({message: 'Deleted Tag'})
        } catch(error) {
            res.status(500).json({message: 'Internal Server Error'})
        }
    }
}

export default TagController