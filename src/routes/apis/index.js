import express from 'express'
import Middleware from '../../middlewares'
import PostValidator from '../../validators/PostValidator'
import PostController from '../../controllers/PostController'
import AuthorValidator from '../../validators/AuthorValidator'
import AuthorController from '../../controllers/AuthorController'
import TagValidator from '../../validators/TagValidator'
import TagController from '../../controllers/TagController'

const router = express.Router()

router.get('/posts', PostController.get)
router.post('/posts', [PostValidator.create(), Middleware.Guest], PostController.create)
router.patch('/posts/:id', [PostValidator.update(), Middleware.Auth], PostController.update)
router.delete('/posts/:id', [PostValidator.delete(), Middleware.Auth], PostController.delete)

router.get('/authors', AuthorController.get)
router.post('/authors', [AuthorValidator.create(), Middleware.Guest], AuthorController.create)
router.patch('/authors/:id', [AuthorValidator.update(), Middleware.Guest], AuthorController.update)
router.delete('/authors/:id', [AuthorValidator.delete(), Middleware.Guest], AuthorController.delete)

router.get('/tags', TagController.get)
router.post('/tags', [TagValidator.create(), Middleware.Guest] , TagController.create)
router.patch('/tags/:id', [TagValidator.update(), Middleware.Guest] , TagController.update)
router.delete('/tags/:id', [TagValidator.delete(), Middleware.Guest] , TagController.delete)


export default router
