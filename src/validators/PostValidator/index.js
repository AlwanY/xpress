import { body, param } from 'express-validator'

class PostValidator {
  static create = () => [
    body('title').optional().isString(),
    body('body').optional().isString(),
    body('authorId').optional().isUUID()
  ]

  static update = () => [
    param('id').exists().isUUID(),
    body('title').optional().isString(),
    body('body').optional().isString(),
    body('authorId').optional().isUUID()
  ]

  static delete = () => [
    param('id').exists().isUUID()
  ]
}

export default PostValidator
